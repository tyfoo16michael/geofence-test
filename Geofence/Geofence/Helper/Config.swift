//
//  Config.swift
//  Geofence
//
//  Created by tyfoo on 23/10/2019.
//  Copyright © 2019 tyf. All rights reserved.
//

import Foundation
import CoreLocation

class Config {
    static let SSID = "Rentasara_5G@unifi"
    
    // Polygon data that covered Seri Gembira Avenue
    // Use Test.gpx for testing if selected this area.
    static let area1: [CLLocationCoordinate2D] = [
        CLLocationCoordinate2D(latitude: 3.080057, longitude: 101.686179),
        CLLocationCoordinate2D(latitude: 3.080086, longitude: 101.687102),
        CLLocationCoordinate2D(latitude: 3.079063, longitude: 101.687123),
        CLLocationCoordinate2D(latitude: 3.079028, longitude: 101.686214)
    ]
    
    // Polygon data that covered Ampang Point
    // Use Test.gpx for testing if selected this area.
    static let area2: [CLLocationCoordinate2D] = [
        CLLocationCoordinate2D(latitude: 3.158299, longitude: 101.750733),
        CLLocationCoordinate2D(latitude: 3.157948, longitude: 101.751461),
        CLLocationCoordinate2D(latitude: 3.157368, longitude: 101.751185),
        CLLocationCoordinate2D(latitude: 3.157705, longitude: 101.750455)
    ]
    
    // Center coordinate of Ampang Point
    static let area3: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 3.158299, longitude: 101.750733)
    static let area3Radius: Double = 50.0
    
}
