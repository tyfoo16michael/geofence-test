//
//  Location.swift
//  Geofence
//
//  Created by tyfoo on 23/10/2019.
//  Copyright © 2019 tyf. All rights reserved.
//

import Foundation
import CoreLocation

class Location {
    let latitude: Double
    let longitude: Double
    let date: Date
    let dateString: String
    let description: String
    
    init(visit: CLVisit, description: String) {
        self.latitude = visit.coordinate.latitude
        self.longitude = visit.coordinate.longitude
        self.date = visit.arrivalDate
        self.dateString = ""
        self.description = description
    }
}
