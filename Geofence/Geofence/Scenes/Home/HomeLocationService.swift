//
//  HomeLocationService.swift
//  Geofence
//
//  Created by tyfoo on 23/10/2019.
//  Copyright © 2019 tyf. All rights reserved.
//

import Foundation
import CoreLocation

protocol HomeLocationServiceDelegate {
    func didUpdateLocation(coordinate: CLLocationCoordinate2D)
}

class HomeLocationService: NSObject {
    
    private var locationManager: CLLocationManager
    
    var delegate: HomeLocationServiceDelegate?
    
    init(locationManager: CLLocationManager) {
        self.locationManager = locationManager
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        requestPermission()
        setupLocationManager()
    }
    
    func requestPermission() {
        locationManager.requestAlwaysAuthorization()
    }
    
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.distanceFilter = 10
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
    }
    
}
extension HomeLocationService: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.first else {
            return
        }
//        print(location.coordinate)
        delegate?.didUpdateLocation(coordinate: location.coordinate)
    }
}
