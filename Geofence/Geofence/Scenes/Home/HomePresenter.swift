//
//  HomePresenter.swift
//  Geofence
//
//  Created by tyfoo on 23/10/2019.
//  Copyright © 2019 tyf. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps
import Darwin

protocol HomeView: NSObjectProtocol {
    func setDetectLabelText(_ text: String)
    func setMapViewInitialLocation(_ location: CLLocation)
    func displaySelectedAreaInMap(_ rect: GMSMutablePath)
    func displaySelectedAreaInMap(_ center: CLLocationCoordinate2D, radius: Double)
    func setSelectedAreaText(_ text: String)
    func updateStatusText(_ text: String)
}

class HomePresenter {
    
    private let area1: [CLLocationCoordinate2D] = Config.area1
    private let area2: [CLLocationCoordinate2D] = Config.area2
    private let area3: CLLocationCoordinate2D = Config.area3
    private let area3Radius: Double = Config.area3Radius
    private let desiredSSID: String = Config.SSID
    weak private var view: HomeView?
    private var service: HomeLocationService?
    private var firstLocationFlag: Bool = true
    private var circleFlag: Bool = false
    
    private var selectedArea: [CLLocationCoordinate2D] = [] {
        didSet {
            let rect = GMSMutablePath()
            for path in selectedArea {
                rect.add(CLLocationCoordinate2D(latitude: path.latitude, longitude: path.longitude))
            }
            view?.displaySelectedAreaInMap(rect)
        }
    }
    
    
    init(service: HomeLocationService) {
        self.service = service
        self.service?.delegate = self
        self.service?.requestPermission()
        self.service?.setupLocationManager()
    }
    
    func attachView(_ view: HomeView){
        self.view = view
    }
    
    func detachView() {
        view = nil
    }
    
    // // MARK: Core functions
    /**
     Check polygon contains the coordinate.
     
     - Parameter polygon: The polygon data consist of CLLocationCoordinate2D array.
     - Parameter point: The coordinate of device or specific coordinate.
     - Returns: Boolean
     */
    private func containsPoint(polygon: [CLLocationCoordinate2D], point: CLLocationCoordinate2D) -> Bool {
        
        let path = GMSMutablePath()
        for p in polygon {
            path.add(p)
        }
        if GMSGeometryContainsLocation(point, path, true) {
            return true
        } else {
            return false
        }
    }
    /**
     Check if circle area contains the coordinate.
     
     - Parameter center: The center coordinate of circle area.
     - Parameter current: The current coordinate of device or specific coordinate.
     - Parameter diameter: diamter value in meter
     - Returns: Boolean
     */
    private func containsPoint(center: CLLocationCoordinate2D, current: CLLocationCoordinate2D, diameter: Double) -> Bool {
        
        let ky = 40000.0 / 360.0;
        let kx = cos(.pi * center.latitude / 180.0) * ky;
        let dx = abs(center.longitude - current.longitude) * kx;
        let dy = abs(center.latitude - current.latitude) * ky;
        return sqrt(dx * dx + dy * dy) <= (diameter/1000.0);

    }
    
    func setSelectedArea(sender: UIButton) {

        switch sender.tag {
//        case 0...1:
//            circleFlag = false
        case 0:
            circleFlag = false
            selectedArea = area1
            view?.setSelectedAreaText("AREA 1")
            break
        case 1:
            circleFlag = false
            selectedArea = area2
            view?.setSelectedAreaText("AREA 2")
            break
        case 2:
            circleFlag = true
            circleAreaSelected()
            view?.setSelectedAreaText("AREA 3")
            break
        default:
            break
        }
    }
    
    /**
     Display circle in map view when circle area selected.
     */
    private func circleAreaSelected() {
        print("Circle selected")
        view?.displaySelectedAreaInMap(self.area3, radius: 50.0)
    }
    
    /**
    Check device WiFi connection status and reflect the changes on view.
    - Parameter ssid: SSID name
    - Returns: Boolean
    */
    private func connectedToWiFi(_ ssid: String) -> Bool {

        #if targetEnvironment(simulator)
            view?.updateStatusText("You are running in Simulator. Please run in real device. The status will remains connected for testing.")
            return true
        #else
            // real device
            if let ssid = currentSSIDs().first, ssid == ssid {
                view?.updateStatusText("You are currently connected to \(ssid)")
                return true
            }
            view?.updateStatusText("You are not connected to \(ssid).\n\nPlease admend the WiFI access point in Helper/Config.swift.")
            return false
        #endif
        
    }
    
}
extension HomePresenter: HomeLocationServiceDelegate {
    
    func didUpdateLocation(location: CLLocation) {
        
        if firstLocationFlag {
            print("Did call")
            view?.setMapViewInitialLocation(location)
            firstLocationFlag = false
        }
        
        let connected = connectedToWiFi(desiredSSID)
        var displayString = ""
        
        if circleFlag {
            let insideCircle = containsPoint(center: Config.area3, current: location.coordinate, diameter: Config.area3Radius)
            if insideCircle {
                displayString = (insideCircle && connected) || connected ? "INSIDE" : "OUTSIDE"
            } else {
                displayString = (insideCircle && connected) || connected ? "INSIDE" : "OUTSIDE"
            }
            view?.setDetectLabelText(displayString)
            print("In cirle: \(insideCircle)")
            return
        }
        
        let insidePolygon = containsPoint(polygon: selectedArea, point: location.coordinate)
        print("In polygon: \(insidePolygon)")
        displayString = (insidePolygon && connected) || connected ? "INSIDE" : "OUTSIDE"
        view?.setDetectLabelText(displayString)
        
    }
}
