//
//  ViewController.swift
//  Geofence
//
//  Created by tyfoo on 23/10/2019.
//  Copyright © 2019 tyf. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class HomeViewController: UIViewController {
    
    @IBOutlet weak var area1Btn: UIButton!
    
    @IBOutlet weak var area2Btn: UIButton!
    
    @IBOutlet weak var area3Btn: UIButton!
    
    @IBOutlet weak var detectLabel: UILabel!
    
    @IBOutlet weak var selectedAreaLabel: UILabel!

    @IBOutlet weak var statusLabel: UILabel!
    
    
    @IBOutlet weak var mapView: GMSMapView!
    
    let presenter = HomePresenter(service: HomeLocationService(locationManager: CLLocationManager()))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.attachView(self)
        setupGestureRecognizers()
        setupMapView()
    }
    
    private func setupGestureRecognizers() {
        area1Btn.tag = 0
        area2Btn.tag = 1
        area3Btn.tag = 2
        area1Btn.addTarget(self, action: #selector(setSelectedArea), for: .touchUpInside)
        area2Btn.addTarget(self, action: #selector(setSelectedArea), for: .touchUpInside)
        area3Btn.addTarget(self, action: #selector(setSelectedArea), for: .touchUpInside)
    }
    
    private func setupMapView() {
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    @objc private func setSelectedArea(sender: UIButton) {
        presenter.setSelectedArea(sender: sender)
    }
    
}
extension HomeViewController: HomeView {
    
    func displayWiFiNotConnected(_ text: String) {
        statusLabel.text = text
    }
    
    func displayWiFiConnected(_ text: String) {
        statusLabel.text = text
    }
    
    func displayInSimulatorMode(_ text: String) {
        statusLabel.text = text
    }
    
    func setSelectedAreaText(_ text: String) {
        selectedAreaLabel.text = text
    }
    
    func setDetectLabelText(_ text: String) {
        detectLabel.text = text
    }
    
    func setMapViewInitialLocation(_ coordinate: CLLocationCoordinate2D) {
        mapView.camera = GMSCameraPosition(target: coordinate, zoom: 17, bearing: 0, viewingAngle: 0)
    }
    
    func displaySelectedAreaInMap(_ rect: GMSMutablePath) {
        mapView.clear()
        let polygon = GMSPolygon(path: rect)
        polygon.fillColor = UIColor(red: 0.95, green: 0, blue: 0, alpha: 0.05);
        polygon.strokeColor = .black
        polygon.strokeWidth = 2
        polygon.map = mapView
        
    }
    
    func displaySelectedAreaInMap(_ center: CLLocationCoordinate2D, radius: Double) {
        mapView.clear()
        let circle = GMSCircle(position: center, radius: radius)
        circle.strokeColor = UIColor.blue
        circle.fillColor = UIColor(red: 0, green: 0, blue: 0.35, alpha: 0.05)
        circle.map = mapView
    }
    
    
}


