# Geofence Test

iOS application that determine whether user's device located inside or out of geofence area.


## Environment
- Xcode 10.2.1
- iOS 10.0 and above
- Swift 5

## Dependencies
### CocoaPods
- GoogleMaps 

## Preview
![](preview.png)

## What to Know

This application purpose is to visualise the user location status when meet conditions stated below:

**OUTSIDE**
- Device location is out of the specific area.

**INSIDE** 
- Device location is within the specific area or device connected to specific WiFi network.

This app provided 3 area for user to choose from, which is Area 1, Area 2 and Area 3. Each of them represent different area at Kuala Lumpur, Malaysia.

* **Area 1** : polygon that covered *Seri Gembira Avenue*.

* **Area 2** : 
polygon that covered *Ampang Point*.

* **Area 3** : circle area that nearby *Ampang Point*.

For the routing, this app use gpx to declare the travel path of the device. You can follow *How to Run* to config the gpx.

* **Test.gpx** : Contains the routing path for *area 1*. 

* **Test2.gpx** :  Contains the routing path for *area 2*  and *area 3*.

*Please take note that Wifi information only can be accessed through real device. You will not able to test the WiFi status in simulator environment*. 


## How to run

1. Set your SSID string in *Helper/Config.swift*.
```swift
class Config {
static let SSID = "xxxx_5GHz@unifi"
}
```

2. In Xcode, go to *Product* > *Scheme* > *Edit Scheme...* or press **Command  <** 

3. Go to **Option** tab, check **Allow Location Simulation** and select Test.gpx. (Select Test2.gpx if you want test on area 2)
![](selectLocation.png)

4. Run the project.
